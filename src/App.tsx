import React from 'react';
import './App.css';
import {Route, Routes} from "react-router-dom";
import Admin from "./components/admin";
import Scoreboard from "./components/scoreboard";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route index element={<Admin />} />
        <Route path="scoreboard" element={<Scoreboard />} />
      </Routes>
    </div>
  );
}

export default App;
