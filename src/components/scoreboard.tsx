import React from 'react'
import useLocalStorage from "../utils/useLocalStorage";
import {DEFAULT_STATE, State} from "./admin";

export default function Scoreboard(){
  const [state, ] = useLocalStorage<State>("state", DEFAULT_STATE);

  return <div>
    <div>{state.homeTeamName}: {state.homeTeamScore} </div><br/>
    <div>{state.awayTeamName}: {state.awayTeamScore}</div>
  </div>
}