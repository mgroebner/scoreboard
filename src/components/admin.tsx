import React, {ChangeEvent} from 'react'
import useLocalStorage from "../utils/useLocalStorage";

export interface State {
  homeTeamName: string
  awayTeamName: string
  homeTeamScore: string
  awayTeamScore: string
}

export const DEFAULT_STATE = {homeTeamName: "", awayTeamName: "", awayTeamScore: "0", homeTeamScore: "0"}

export default function Admin(){
  const [state, setState] = useLocalStorage<State>("state", DEFAULT_STATE);


  function handleHomeTeamChange(event: ChangeEvent<HTMLInputElement>) {
    setState(prevState => ({...prevState, homeTeamName: event.target.value}))
  }

  function handleAwayTeamChange(event: ChangeEvent<HTMLInputElement>) {
    setState(prevState => ({...prevState, awayTeamName: event.target.value}))
  }

  function handleHomeTeamScoreChange(event: ChangeEvent<HTMLInputElement>) {
    setState(prevState => ({...prevState, homeTeamScore: event.target.value}))
  }

  function handleAwayTeamScoreChange(event: ChangeEvent<HTMLInputElement>) {
    setState(prevState => ({...prevState, awayTeamScore: event.target.value}))
  }

  return <div>
    <div>home team: <input value={state.homeTeamName} onChange={handleHomeTeamChange}/> <input value={state.homeTeamScore} onChange={handleHomeTeamScoreChange}/></div><br/>
    <div>away team: <input value={state.awayTeamName} onChange={handleAwayTeamChange}/> <input value={state.awayTeamScore} onChange={handleAwayTeamScoreChange}/></div>
    <button onClick={() => window.open('/scoreboard','ScoreboardWindow','width=600,height=300')}>open scoreboard</button>
  </div>
}